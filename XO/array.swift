
struct ArrayXo<T> {
    
    let columns: Int;
    let rows : Int;
    private var array:Array<T?>
    
    init(columns:Int, rows:Int){
        self.columns = columns;
        self.rows = rows;
        self.array = Array<T?>(count: columns * rows, repeatedValue: nil);
        
    }
    subscript(column : Int, row : Int) ->T?{
        get{
        return array[row * columns + column]
    }
        set{
            array[row * columns + column] = newValue
            
        }
    
    }
    
    
}

