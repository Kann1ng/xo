import SpriteKit

class GameScene: SKScene {
    
    let board = SKNode();
    let SizeOfTile : CGFloat = 90.0;
    
    var level : Level!;
    
    override func didMoveToView(view: SKView) {
        
        self.anchorPoint = CGPoint(x:0.5, y:0.5);
       
        var BoardPos = CGPoint(x : -SizeOfTile * CGFloat(ColCount)/2,
                y : -SizeOfTile * CGFloat(RowCount)/2 );
        
        board.position = BoardPos;
        
        
        self.addChild(board);
        AddBoard();
        
        
    }
    
    
    func rectangleNumber(point : CGPoint)->( column : Int, row : Int ){
        return (Int( point.x / SizeOfTile ), Int(point.y / SizeOfTile)) ;
        
    }
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
    
        let touch = touches.first as! UITouch;
        
        let location = touch.locationInNode(board);
        
        var( column, row ) = rectangleNumber(location);
        
        var sym = level.currentSymbol;
        print(sym)
        let symbol = SKSpriteNode( imageNamed: sym );
        
        
        let symbolLocation = PoinForXY(column, row: row);
    
        
        symbol.position = symbolLocation;
        
        if (location.x >= 0 && location.x < SizeOfTile * CGFloat( ColCount ) && location.y >= 0 && location.y < SizeOfTile * CGFloat( RowCount ))
        { board.addChild(symbol);
        
        }
        
        
        
        
    }
   
    override func update(currentTime: CFTimeInterval) {
       
    }
    
    func PoinForXY( column : Int, row : Int )->CGPoint{
        return CGPoint(x : CGFloat( column ) * SizeOfTile + SizeOfTile/2, y : CGFloat(row) * SizeOfTile + SizeOfTile/2);
        
    }
    func AddBoard(){
        for row in 0..<RowCount{
            for col in 0..<ColCount{
                let tile = SKSpriteNode(imageNamed: "tile");
                tile.position = PoinForXY(col, row : row);
                board.addChild(tile);
                
            }
        }
    }
    
}
